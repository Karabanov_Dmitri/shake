package com.example.snake;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.Random;

@SuppressLint("ViewConstructor")
public class SnakeEngine extends SurfaceView implements Runnable {
    private long frameTime;
    private long nextTime;
    private long time;
    private Thread thread = null;
    private SoundPool soundPool;
    private int eat_apple = -1;
    private int snake_crash = -1;

    public enum Heading {UP, RIGHT, DOWN, LEFT}

    private Heading heading = Heading.RIGHT;
    private int screenX;
    private int screenY;
    private int snakeLength;
    private int appleX;
    private int appleY;
    private int blockSize;
    private final int NUM_BLOCKS_WIDE = 40;
    private int numBlocksHigh;
    private long nextFrameTime;
    private final long FPS = 7;
    private final long MILLIS_PER_SECOND = 1000;
    private int score;
    private int[] snakeXs;
    private int[] snakeYs;
    private volatile boolean isPlaying;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;
    private Paint paint;


    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public SnakeEngine(Context context, Point size) {
        super(context);
        screenX = size.x;
        screenY = size.y;

        //какого размера блок и сколько их в высоте
        blockSize = screenX / NUM_BLOCKS_WIDE;
        numBlocksHigh = screenY / blockSize;

        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        try {
            AssetManager assetManager = context.getAssets();
            AssetFileDescriptor descriptor;

            descriptor = assetManager.openFd("eat_apple.ogg");
            eat_apple = soundPool.load(descriptor, 0);

            descriptor = assetManager.openFd("snake_crash.ogg");
            snake_crash = soundPool.load(descriptor, 0);

        } catch (IOException ignored) {

        }

        surfaceHolder = getHolder();
        paint = new Paint();

        snakeXs = new int[200];
        snakeYs = new int[200];


        newGame();
    }

    /**
     * Метод запуска новой игры
     */
    public void newGame() {
        frameTime = System.currentTimeMillis();
        snakeLength = 1;
        snakeXs[0] = NUM_BLOCKS_WIDE / 2;
        snakeYs[0] = numBlocksHigh / 2;

        spawnApple();

        score = 0;
        nextFrameTime = System.currentTimeMillis();
    }

    /**
     * Метод создания яблочка
     */
    public void spawnApple() {
        Random random = new Random();
        appleX = random.nextInt(NUM_BLOCKS_WIDE - 1) + 1;
        appleY = random.nextInt(numBlocksHigh - 1) + 1;
    }

    /**
     * Метод осуществления действия при съедении яблочка
     */
    private void eatApple() {
        snakeLength++;

        spawnApple();

        score = score + 1;
        soundPool.play(eat_apple, 1, 1, 0, 0, 1);
    }

    /**
     * Метод перемещения змеи
     */
    private void moveSnake() {
        for (int i = snakeLength; i > 0; i--) {

            snakeXs[i] = snakeXs[i - 1];
            snakeYs[i] = snakeYs[i - 1];
        }

        switch (heading) {
            case UP:
                snakeYs[0]--;
                break;

            case RIGHT:
                snakeXs[0]++;
                break;

            case DOWN:
                snakeYs[0]++;
                break;

            case LEFT:
                snakeXs[0]--;
                break;
        }
    }

    /**
     * Метод обнаружения столкновения в края экрана или тело змеи
     *
     * @return dead
     */
    private boolean detectDeath() {
        boolean dead = false;

        if (snakeXs[0] == -1) dead = true;
        if (snakeXs[0] >= NUM_BLOCKS_WIDE) dead = true;
        if (snakeYs[0] == -1) dead = true;
        if (snakeYs[0] == numBlocksHigh) dead = true;

        for (int i = snakeLength - 1; i > 0; i--) {
            if ((i > 4) && (snakeXs[0] == snakeXs[i]) && (snakeYs[0] == snakeYs[i])) {
                dead = true;
            }
        }

        return dead;
    }

    @Override
    public void run() {
        while (isPlaying) {
            if (updateRequired()) {
                update();
                draw();
            }
        }
    }

    /**
     * Метод управляющий частотой кадров в игре
     *
     * @return true or false
     */
    public boolean updateRequired() {
        if (nextFrameTime <= System.currentTimeMillis()) {
            nextFrameTime = System.currentTimeMillis() + MILLIS_PER_SECOND / FPS;
            return true;
        }
        return false;
    }

    /**
     * Метод рисования игры
     */
    public void draw() {
        if (surfaceHolder.getSurface().isValid()) {
            canvas = surfaceHolder.lockCanvas();

            canvas.drawColor(Color.argb(255, 26, 128, 182));
            paint.setColor(Color.argb(255, 255, 255, 255));
            nextTime = System.currentTimeMillis();
            paint.setTextSize(50);
            canvas.drawText("Score:" + score, 10, 50, paint);
            canvas.drawText("Time:" + updateTimer(), 10, 100, paint);

            for (int i = 0; i < snakeLength; i++) {
                canvas.drawRect(snakeXs[i] * blockSize,
                        (snakeYs[i] * blockSize),
                        (snakeXs[i] * blockSize) + blockSize,
                        (snakeYs[i] * blockSize) + blockSize,
                        paint);
            }
            paint.setColor(Color.argb(255, 255, 0, 0));

            canvas.drawRect(appleX * blockSize,
                    (appleY * blockSize),
                    (appleX * blockSize) + blockSize,
                    (appleY * blockSize) + blockSize,
                    paint);

            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    /**
     * Метод управления змейкой
     *
     * @param motionEvent касание
     * @return направление движения
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                if (motionEvent.getX() >= screenX / 2) {
                    switch (heading) {
                        case UP:
                            heading = Heading.RIGHT;
                            break;
                        case RIGHT:
                            heading = Heading.DOWN;
                            break;
                        case DOWN:
                            heading = Heading.LEFT;
                            break;
                        case LEFT:
                            heading = Heading.UP;
                            break;
                    }
                } else {
                    switch (heading) {
                        case UP:
                            heading = Heading.LEFT;
                            break;
                        case LEFT:
                            heading = Heading.DOWN;
                            break;
                        case DOWN:
                            heading = Heading.RIGHT;
                            break;
                        case RIGHT:
                            heading = Heading.UP;
                            break;
                    }
                }
        }
        return true;
    }

    /**
     * Метод обновления
     */
    public void update() {
        if (snakeXs[0] == appleX && snakeYs[0] == appleY) {

            eatApple();
        }

        moveSnake();

        if (detectDeath()) {
            soundPool.play(snake_crash, 1, 1, 0, 0, 1);

            newGame();
        }
    }

    public void pause() {
        isPlaying = false;
        try {
            thread.join();
        } catch (InterruptedException ignored) {

        }
    }

    public void resume() {
        isPlaying = true;
        thread = new Thread(this);
        thread.start();
    }

    public String updateTimer() {
        time = nextTime - frameTime;
        int min = (int) time / 60000;
        int sec = (int) time % 60000 / 1000;
        String timeLeftText;
        timeLeftText = "" + min + ":";
        if (sec < 10) timeLeftText += "0";
        timeLeftText += sec;
        return timeLeftText;
    }
}
